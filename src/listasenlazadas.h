#ifndef LISTASENLAZADAS_H
#define LISTASENLAZADAS_H
//PELAYO LEGUINA LOPEZ, UO246774


/* MODIFICACIONES :
   --------------

 * He modificado la funcion mostrar_lista, haciendola (void) ya que me parece mas comodo y que puede optimizar mejor el programa.


*/




typedef struct _nodo
{
    int ident;
    int valor;
    struct _nodo *siguiente;
} T_NODO;

typedef struct _nodo * T_LISTA;

T_NODO * crear_nodo(int ident, int valor);

T_LISTA insertar_al_principio(T_LISTA lista, T_NODO *nodo);
T_LISTA insertar_al_final(T_LISTA lista, T_NODO *nodo);
T_LISTA insertar_ordenado(T_LISTA lista, T_NODO *nodo);

T_LISTA eliminar_primero(T_LISTA lista);
T_LISTA eliminar_ultimo(T_LISTA lista);
T_LISTA eliminar_identificador(T_LISTA lista, int ident);

void mostrar_lista(T_LISTA lista);

int longitud_lista(T_LISTA lista);





#endif // LISTASENLAZADAS_H
