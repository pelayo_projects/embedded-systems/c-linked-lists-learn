#include "listasenlazadas.h"
#include <stdio.h>
//PELAYO LEGUINA LOPEZ, UO246774


int main()
{
    T_LISTA lista=NULL;
    T_NODO *p_nodo;

    printf("Insertamos nodo (5,10) al principio\n");
    p_nodo = crear_nodo(5, 10);
    lista = insertar_al_principio(lista, p_nodo);


    printf("Insertamos nodo (8,13) al final\n");
    p_nodo = crear_nodo(8, 13);
    lista = insertar_al_final(lista, p_nodo);


    printf("Insertamos nodo (7,1) entre los dos\n");
    p_nodo = crear_nodo(7, 1);
    lista = insertar_ordenado(lista, p_nodo);



    printf("Insertamos nodo (1,4) en orden\n");
    p_nodo = crear_nodo(1, 4);
    lista = insertar_ordenado(lista, p_nodo);


    printf("Insertamos nodo (6,99) en orden\n");
    p_nodo = crear_nodo(6, 99);
    lista = insertar_ordenado(lista, p_nodo);


    printf("Insertamos nodo (20,9) en orden\n");
    p_nodo = crear_nodo(20, 9);
    lista = insertar_ordenado(lista, p_nodo);

    mostrar_lista(lista);


    int a = longitud_lista(lista);

    printf("-- La longitud de la lista es de %i elementos \n",a);


    printf(" \n ************* \n");
    printf("\n Procedo a eliminar elementos: \n");
    printf(" \n ************* \n");

    /* ELIMINO ALGUNOS */

    printf("\n Elimino primero, ultimo y nodo con ident = 6 y nodo con ident = 48 (no existe) \n");
    lista = eliminar_ultimo(lista);

    lista = eliminar_primero(lista);

    lista = eliminar_identificador(lista,6);

    lista = eliminar_identificador(lista,48);

    mostrar_lista(lista);

    a = longitud_lista(lista);

    printf("-- La longitud de la lista es de %i elementos \n",a);

    /*EXCEPCIONES*/

    printf(" \n ************* \n");
    printf("\n Procedo a probar excepciones: \n");
    printf(" \n ************* \n");

    printf(" \n Reinicio la lista \n");

    lista = NULL;
    mostrar_lista(lista);


    printf(" \n Compruebo si la funcion insertar ordenado funciona si la lista esta vacia \n");
    printf("Insertamos nodo (6,99) \n");
    p_nodo = crear_nodo(6, 99);
    lista = insertar_ordenado(lista, p_nodo);
    mostrar_lista(lista);


    printf(" \n Compruebo si la funcion eliminar funciona si la lista esta vacia \n"
           "(Lo compruebo una vez ya que tengo la misma condicion en todas las funciones)\n"
           " *************************************************************************\n");


    lista = eliminar_identificador(lista,6);
    lista = eliminar_identificador(lista,7);

    a = longitud_lista(lista);

    printf("-- La longitud de la lista es de %i elementos \n",a);

    printf(" \n ************* \n");
    printf("\n FIN DE PROGRAMA\n"
           ""
           ""
           "PELAYO LEGUINA LOPEZ"
           "     14/04/2020 \n");
    printf(" \n ************* \n");


    return 0;
}
