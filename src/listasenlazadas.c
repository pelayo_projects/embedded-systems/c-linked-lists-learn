#include "listasenlazadas.h"

#include <stdio.h>
#include <stdlib.h>

//PELAYO LEGUINA LOPEZ, UO246774

/* MODIFICACIONES :
   --------------

 * He modificado la funcion mostrar_lista, haciendola (void) ya que me parece mas comodo y que puede optimizar mejor el programa.


*/

T_NODO * crear_nodo(int ident, int valor)
{
    T_NODO *p_nodo; // crear una variable para guardar el puntero a la zona de memoria reservada para el nodo

    p_nodo = malloc(sizeof(T_NODO));

    if (p_nodo != NULL)
    {
        p_nodo->ident = ident;
        p_nodo->valor = valor;
        p_nodo->siguiente = NULL;
    }
    return p_nodo;
}

T_LISTA insertar_al_principio(T_LISTA lista, T_NODO *nodo)
{
    if (nodo != NULL)
    {
        nodo->siguiente = lista;
        lista = nodo;
    };

    return lista;

}

T_LISTA insertar_al_final(T_LISTA lista, T_NODO *nodo)
{

    T_NODO *aux;
    aux = lista;

    if(nodo!=NULL)
    {
        if(lista == NULL)
        {
            lista =  nodo;
        }
        else
        {
            while(aux->siguiente != NULL)
            {
                aux = aux->siguiente;
            }

            aux->siguiente = nodo;
        }
    }

    return lista;

}

void mostrar_lista(T_LISTA lista)
{

    T_NODO *aux;

    aux = lista;
    if (aux != NULL)
    {

        while(aux != NULL)
        {
            printf(" ( %i,%i ) ->",aux->ident,aux->valor);

            aux = aux->siguiente;
        }
        printf(" FIN ");
    }
    else
    {
        printf("Lista Vacia ");
    }
}

T_LISTA insertar_ordenado(T_LISTA lista, T_NODO *nodo)
{
    T_NODO *aux; // Creamos un nodo auxiliar

    aux = lista; // Para no sobreescribir lista


    if(lista == NULL) //Compruebo antes si la lista esta vacia
    {
        lista = nodo;
    }
    else if(nodo->ident <= aux->ident) //Si el nodo que queremos introducir tiene un indice mas pequeño que el primer elemento de la lista, lo insertara al principio.
    {
        lista = insertar_al_principio(lista, nodo);
    }
    else //Si el indice del nodo es mayor puede ocurrir que queramos introducirlo por el medio o al final
    {

        while (1) // Recorro toda la lista
        {
            if(aux->siguiente == NULL) //Si el siguiente nodo esta vacio significa que estoy en el ultimo nodo de la lista
            {
                lista = insertar_al_final(lista, nodo);
                break;
            }
            else if(aux->ident <= nodo->ident && aux->siguiente->ident >= nodo->ident) //Si el indice del nodo se encuentra entre el indice del elemento de la lista inmediatamente mas pequeño y el indice del elemento siguiente inmediatamente mas grande entonces:
            {

                nodo->siguiente = aux->siguiente; //El nuevo nodo apuntara al siguiente elemento de la lista
                aux->siguiente = nodo;
                break;
            }
            else aux = aux->siguiente;
        }

    }

    return lista;
}




/* Eliminar elementos de listas */

/* (Siempre compruebo si la lista se encuentra vacia antes de realizar la operacion) */






T_LISTA eliminar_primero(T_LISTA lista) //Hago que la lista apunte al siguiente nodo y despues libero el primero
{
    T_NODO *aux;

    if(lista == NULL)
    {
        printf("****La lista esta vacia, no hay nada que eliminar...****");
    }
    else
    {
        aux = lista;

        lista = aux->siguiente;

        free(aux);
    }

    return lista;

}

T_LISTA eliminar_ultimo(T_LISTA lista)
{
    T_NODO *aux;
    T_NODO *prev;

    if(lista == NULL)
    {
        printf("****La lista esta vacia, no hay nada que eliminar...****");
    }
    else
    {

        aux = lista;

        if (aux->siguiente == NULL)
        {
            lista = NULL;
            return lista;
        }

        while(aux->siguiente)
        {
            prev = aux;
            aux = aux->siguiente;
        }
        prev->siguiente = NULL;
        free(aux);

    }



    return lista;
}

T_LISTA eliminar_identificador(T_LISTA lista, int ident)
{
    T_NODO *aux;
    T_NODO *prev = NULL;

    if(lista == NULL)
    {
        printf("****La lista esta vacia, no hay nada que eliminar...****");
    }
    else
    {

        aux = lista;

        while (1) //4 condiciones
        {
            if(aux->ident == ident && aux->siguiente != NULL && prev == NULL) //Si el elemento es el primero
            {
                lista=eliminar_primero(lista);
                break;
            }
            else if (aux->ident == ident && aux->siguiente != NULL && prev != NULL) //Si esta por el medio, ni al principio ni al final.
            {
                prev->siguiente = aux->siguiente;
                free(aux);
                break;
            }

            else if(aux->siguiente == NULL && aux->ident != ident) //Si ha recorrido la lista entera y no ha encontrado ningun nodo para eliminar
            {
                break;
            }

            else if (aux->ident == ident && aux->siguiente == NULL) //Si el elemento a eliminar es el ultimo
            {
                lista=eliminar_ultimo(lista);
                break;
            }




            prev = aux;
            aux = aux->siguiente; //Avanzamos por la lista
        }

    }


    return lista;
}



/* LONGITUD DE LA LISTA */

int longitud_lista(T_LISTA lista)
{
    int idx =0;
    while(lista!= NULL)
    {
        lista = lista->siguiente;
        idx++;
    }
    return idx;
}
